def jet_theme():
    JET_THEMES = [
        {
            'theme': 'default',  # theme folder name
            'color': '#47bac1',  # color of the theme's button in user menu
            'title': 'Default'  # theme title
        },
        {
            'theme': 'green',
            'color': '#44b78b',
            'title': 'Green'
        },
        {
            'theme': 'light-green',
            'color': '#2faa60',
            'title': 'Light Green'
        },
        {
            'theme': 'light-violet',
            'color': '#a464c4',
            'title': 'Light Violet'
        },
        {
            'theme': 'light-blue',
            'color': '#5EADDE',
            'title': 'Light Blue'
        },
        {
            'theme': 'light-gray',
            'color': '#222',
            'title': 'Light Gray'
        }
    ]
    return JET_THEMES


def custom_menu():
    JET_SIDE_MENU_ITEMS = {
        'admin': [
            {'label': 'employee', 'app_label': 'employees', 'items': [
                {'name': 'help.question'},
                {'name': 'pages.page'},
                {'name': 'city'},
                {'name': 'validationcode'},
            ]}, ]}
    return JET_SIDE_MENU_ITEMS
