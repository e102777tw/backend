
import os
import MySQLdb
from .import jet_setting
import datetime
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'kzc=3&1e!(e6juyc(%8z8pa2$fud#aeh%d-l*672*73wa^6f46'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            '()': 'django.utils.log.ServerFormatter',
            'format': '[%(asctime)s][%(pathname)s:%(lineno)d]''[%(levelname)s][%(message)s]'
        }
    },
    'handlers': {
        'standard_handler': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'standard',
            }
    },
    'loggers': {
        'standard_logger': {
            'handlers': ['standard_handler'],
            'level':'INFO',
            'propagate': True
        },
    }
}


ALLOWED_HOSTS = ['*']#140.125.45.161','140.125.45.158','140.125.45.156','127.0.0.1

CORS_ORIGIN_ALLOW_ALL = True

# Application definition

INSTALLED_APPS = [
    'jet.dashboard',
    'jet',
    'rest_framework',
    'rest_framework_swagger',
    'admin_chart',
    'employees',
    'information',
    'sinter_process',
    'form_process',
    'qc_2f',
    'linebotchannel',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

]

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'Djet.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates').replace('\\', '/')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'Djet.wsgi.application'


AUTH_USER_MODEL = 'employees.EmployeeAccount'

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'OPTIONS': {
            'sql_mode': 'traditional',
            "init_command": "SET storage_engine=INNODB",
            "init_command":"SET foreign_key_checks = 0;",
        },
        'NAME': 'mysqltest',
        'USER': 'root',
        'PASSWORD': '',
        'HOST': '140.125.45.158',
        'PORT': '3306',
    }
}


# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

APPEND_SLASH = False


# iframe setting
X_FRAME_OPTIONS = 'SAMEORIGIN'


# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/


LANGUAGE_CODE = 'zh-Hant'

TIME_ZONE = 'Asia/Taipei'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.0/howto/static-files/

STATIC_URL = '/static/'


# jet setting
JET_THEMES = jet_setting.jet_theme()  # 主題
JET_SIDE_MENU_COMPACT = True


# Rest
REST_FRAMEWORK = {
    'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema',
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),

    'DEFAULT_AUTHENTICATION_CLASSES': (

        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
    ),
}

JWT_AUTH = {
    'JWT_EXPIRATION_DELTA': datetime.timedelta(minutes=180),
    'JWT_RESPONSE_PAYLOAD_HANDLER': 'employees.views.jwt_response_payload_handler',
}


REST_USE_JWT = True

#line

LINE_ACCESS_TOKEN = "M3itw70gqt1PtAVORF4C4Y5A07jc8yflxDbSqhSN/t0qLA9p2Z331hGMqota8I2xwXCPE7FkMUNGc8GFpTG5EcYr4jdCsoM/lo7XFhpQuz7rkhK5dYULmWcp8HizxvSVxvT5Ck4TK7nZ/5yIewfVmAdB04t89/1O/w1cDnyilFU="
CHANNEL_SECRET = "571954750dc0229224f96a5d81646fa2"



