"""Djet URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from linebotchannel.views import webhook
from rest_framework_swagger.views import get_swagger_view
from report_form.views import data_form
from django.views.generic import TemplateView
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token
from django.contrib import admin
from django.urls import path, include
from sinter_process.views import measure_data_sinter
from qc_2f.views import measure_data_qc_2f_size, measure_data_qc_2f_en
from form_process.views import measure_data_form  
from information.views import (post_product_data, get_product_group, get_customers_name,
get_transfer_doc_master_name, get_transfer_doc_TF_child_name,
get_transfer_doc_TS_child_name, get_transfer_doc_child_name,
Product)
# for input testing data 
from information.read_data_json import input_data_form,create_sinter_size
# swagger


schema_view = get_swagger_view(title='API')


urlpatterns = [

    path('jet/', include('jet.urls', 'jet')),  # Django JET URLS
    path('jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    path('admin/', admin.site.urls, name='admin'),
    path('admin/employees/', include('employees.urls')),
    path('api-auth/', include('rest_framework.urls')),
    path('admin/docs/', schema_view),


    path('sinter-data', measure_data_sinter),
    path('form-data', measure_data_form),
    path('qc-2f-size', measure_data_qc_2f_size),
    path('qc-2f-en', measure_data_qc_2f_en),
    path('create-sinter-size', create_sinter_size),

    path('report-form', data_form),

    path('employee-login', obtain_jwt_token),
    path('api-token-refresh', refresh_jwt_token),
    path('employee-verify', verify_jwt_token),

    path('product-group', get_product_group),
    path('product-data', post_product_data),


    path('customers-name', get_customers_name),
    path('transfer-doc_name', get_transfer_doc_master_name),
    path('transfer-doc-child', get_transfer_doc_child_name),
    path('transfer-doc_tf-child', get_transfer_doc_TF_child_name),
    path('transfer-doc-ts-child', get_transfer_doc_TS_child_name),



    path('input-data-form', input_data_form),
    # line
    path("line/", webhook),

]
