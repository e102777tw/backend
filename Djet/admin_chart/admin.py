from django.contrib import admin
from .models import SinterProcessEmployeeChart
from sinter_process.models import SinterProcess
from employees.models import EmployeeAccount
from employees.serializers import EmployeeSerializer
#for extra_data
from django.db.models.functions import ExtractDay
from django.db.models import Count
import json
from django.core.serializers.json import DjangoJSONEncoder

# Register your models here.
class SinterProcessEmployeeChartProfile(admin.ModelAdmin):
    def chart(self , obj):
        return obj.chart
    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    
    def changelist_view(self, request ,extra_context=None):
        
        chart_data = (
             SinterProcess.objects.values("employee").annotate(y=Count("sinter_process")).order_by("employee_id")
         )

        employee_name=EmployeeAccount.objects.all().order_by("id")
        serializer=EmployeeSerializer(employee_name,many=True )
        name=serializer.data        
       
        employee_name=json.dumps(list(name), cls=DjangoJSONEncoder)

        main_data= json.dumps(list(chart_data), cls=DjangoJSONEncoder)

        name_dict = json.loads(employee_name)
        data_dict = json.loads(main_data)
        count=0
        element=0
        for i in name_dict:
            try:
                
                if(str(name_dict[element]['id'])==str(data_dict[element]['employee'])):
                    data_dict[element]['employee']=name_dict[element]['employee']
                    count=count+1
                    element=element+1
                else:
                    data_dict.insert(element,name_dict[element])
                    data_dict[element]['employee']=name_dict[element]['employee']
                    data_dict[element]['y']="0"
                    element=element+1
            except:
                break
        print(data_dict[element])
        data_dict= json.dumps(list(data_dict), cls=DjangoJSONEncoder)
        extra_context = extra_context or {"employee_chart_data": data_dict}
        print(extra_context)
        return super().changelist_view(request ,extra_context=extra_context)
    
    
admin.site.register(SinterProcessEmployeeChart,SinterProcessEmployeeChartProfile)
