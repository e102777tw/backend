from django.apps import AppConfig


class AdminChartConfig(AppConfig):
    name = 'admin_chart'
    verbose_name='圖表'
    main_menu_index = 2
