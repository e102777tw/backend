from django.contrib import admin
# translation
from django.utils.translation import gettext_lazy as _
# model
from .models import EmployeeAccount
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group


class EmployeeAccountProfile(admin.ModelAdmin):

    change_list_template = 'admin/employees/EmployeeAccount/change_list.html'
    readonly_fields = ['username', 'password', 'line_uid']
    list_display = ['name', 'username',
                    'line_uid', 'job_position', 'is_superuser']
    search_fields = ('name', 'username')
    ordering = ['is_superuser', ]

    def get_fieldsets(self, request, obj=None):

        if request.user.is_superuser:
            perm_fields = ('is_active', 'is_staff',
                           'is_superuser', 'job_position')
        else:

            perm_fields = ('is_active', 'is_staff')

        return [(_('帳號密碼'), {'fields': ('username', 'password', 'name')}),
                (_('Permissions'), {'fields': perm_fields}), ]

    def has_add_permission(self, request, obj=None):
        return False


admin.site.register(EmployeeAccount, EmployeeAccountProfile)
admin.site.unregister(Group)
