# forms
from django import forms
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
# user model
from .models import EmployeeAccount
from .jobs import JOBS_CHOICES

# user create form


class UserCreationForm(UserCreationForm):

    name = forms.CharField(required=True, label='姓名')
    position = forms.ChoiceField(
        required=True, label='職位', choices=JOBS_CHOICES)

    class Meta:
        model = EmployeeAccount
        fields = ('name', 'username', 'position', 'password1', 'password2',)

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.name = self.cleaned_data["name"]
        if commit:
            user.save()
        return user

# user change password form


class PasswordChangeForm(PasswordChangeForm):
    name = forms.CharField(required=True, label='姓名')
    username = forms.CharField(required=True, label='使用者名稱')
    fields_order = ('name', 'username', 'old_password',
                    'new_password1', 'new_password2')
