from django.db import models
# translation
from django.utils.translation import gettext_lazy as _
# User
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.contrib.auth.models import PermissionsMixin
from .manager import UserManager
# 職稱
from .jobs import JOBS_CHOICES


class EmployeeAccount(AbstractBaseUser, PermissionsMixin):

    username_validator = UnicodeUsernameValidator()
    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_(
            'Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },

    )

    password = models.CharField(_('password'), max_length=128,
                                help_text=_('需更改帳密請至XXXX'),)

    is_staff = models.BooleanField(
        _('是否可登入管理後臺'),
        default=False,
        help_text=_(
            'Designates whether the user can log into this admin site.'),
    )

    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )

    is_superuser = models.BooleanField(
        _('最高權限'),
        default=False,
        help_text=_('指定其他使用者是否可以登入到這個管理網站'),
    )

    name = models.CharField(_('姓名'), max_length=30, error_messages={
        'unique': _("A user with that username already exists."),
    })

    line_uid = models.CharField(max_length=120, verbose_name="lineID")
    job_position = models.CharField(
        max_length=120, verbose_name="職稱", choices=JOBS_CHOICES)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['name']
    objects = UserManager()

    def __str__(self):
        return self.username

    class Meta:
        verbose_name_plural = "員工帳號"
        verbose_name = "員工帳號"
