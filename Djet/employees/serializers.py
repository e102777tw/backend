from rest_framework import serializers
from .models import EmployeeAccount


class EmployeeSerializer(serializers.ModelSerializer):

    class Meta:
        model = EmployeeAccount
        fields = ['username', 'name', ]
