from django.urls import path
from . import views
from .views import get_all_employees
app_name = 'employees'

urlpatterns = [

    path('signup', views.registerForm, name='register'),
    path('change-password', views.change_password, name='change_password'),
    path('get-all-employees',get_all_employees)

]
