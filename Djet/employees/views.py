# permission
from django.contrib.auth.decorators import login_required
from rest_framework.decorators import permission_classes
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
# account model
from .models import EmployeeAccount
from .serializers import EmployeeSerializer
# forms
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.hashers import make_password
from django.shortcuts import render, redirect
from employees.forms import UserCreationForm, PasswordChangeForm
from django.http import JsonResponse


# 註冊 register
@login_required
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def registerForm(request):

    form = UserCreationForm(request.POST or None, request.FILES or None)

    if request.method == "POST":

        data = UserCreationForm(request.POST or None, request.FILES or None)
        if data.is_valid():
            clean_data = data.cleaned_data
            user = EmployeeAccount()
            user.username = clean_data.get('username')
            user.password = make_password(
                clean_data.get('password1'), None, 'pbkdf2_sha256')
            user.name = clean_data.get('name')
            user.job_position = clean_data.get('position')
            user.save()
            messages.success(request, '創建員工成功')
            return redirect('/admin/employees/employeeaccount/')
        else:
            error = data.errors
            messages.error(request, '請修正下列錯誤')
    return render(request, 'admin/employees/signup.html', {'form': form})


# 更改密碼 change_password
@login_required
@api_view(['POST'])
@permission_classes([IsAuthenticated])
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)

        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request, '密碼修改成功')
            return redirect('/admin/employees/employeeaccount/')
        else:
            messages.error(request, '請確認以下問題是否正確')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'admin/employees/change_password.html', {'form': form})


# token custom
def jwt_response_payload_handler(token, user=None, request=None):
    try :
        position = user.get_job_position_display()
        user_json = EmployeeSerializer(user, context={'request': request}).data
        user_json['position'] = position
        return {
            'token': token,
            'user': user_json
        }
    except:
        print('jwt_response_payload_handler_failed')



@api_view(['POST'])
@permission_classes([IsAuthenticated])
def get_all_employees(request):
    employees_json = list(EmployeeAccount.objects.all().values_list('name'))
    return JsonResponse(employees_json, safe=False )