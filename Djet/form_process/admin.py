from django.contrib import admin
from form_process.models import FormProcess, FormProcessData, Machine
from employees.models import EmployeeAccount
from django.urls import reverse
from django.utils.html import format_html

@admin.register(Machine)
class MachineProfile(admin.ModelAdmin):
    list_display = [
        field.name for field in Machine._meta.fields if field.name != 'id' if field.name != 'id']

    def machine(self, obj):
        return obj.machine


@admin.register(FormProcessData)
class FormProcessDataProfile(admin.ModelAdmin):
    list_display = [
        # field.name for field in FormProcessData._meta.fields if field.name != "id"]
        field.name for field in FormProcessData._meta.fields ]
    fieldsets = (
        (
            "General Information",
            {
                "fields": (
                    ("product",),
                    ("Aup", "Adown",),
                    ("Bup", "Bdown",),
                ),
            },
        ),
    )

    def product(self, obj):
        return obj.form_process


@admin.register(FormProcess)
class FormProcessProfile(admin.ModelAdmin):

    date_hierarchy = 'check_time'
    list_display = [field.name for field in FormProcess._meta.fields ]



    readonly_fields = [
        field.name for field in FormProcess._meta.fields if field.name != 'id']


    def has_add_permission(self, requsest, obj=None):
        return False
