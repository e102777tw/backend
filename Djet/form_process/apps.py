from django.apps import AppConfig


class FormProcessConfig(AppConfig):
    name = 'form_process'
    verbose_name='  壓形階段'
    main_menu_index = 0