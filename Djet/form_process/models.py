from django.db import models
from django.core.validators import (
    MaxValueValidator,
    MinValueValidator,
)
import django.utils.timezone as timezone
from employees.models import EmployeeAccount




class Machine(models.Model):

    machine = models.CharField(
        max_length=100, verbose_name="壓型機器", default=None)
    machine_serial = models.CharField(
        max_length=50, verbose_name="機器編號", default=None)
    COTTOR_CHOICES = zip(range(1, 4), range(1, 4))
    cottor_number = models.PositiveIntegerField(
        default=1,
        validators=[MaxValueValidator(3), MinValueValidator(1)],
        verbose_name="模具刀數",
        choices=COTTOR_CHOICES,
    )

    class Meta:
        verbose_name_plural = "    壓型機器"
        verbose_name = "壓型機器"

    def __str__(self):
        return self.machine


class FormProcessData(models.Model):


    form_process=models.ForeignKey("form_process.FormProcess",on_delete=models.CASCADE,null=True,related_name='fp_data')
    product = models.ForeignKey("information.Product",verbose_name="規格",on_delete=models.CASCADE,null=True,related_name="fp_product")

    Aup = models.FloatField(
        max_length=100, default=0, blank=True, verbose_name="壓高上限"
    )  # 上下限
    Adown = models.FloatField(
        max_length=100, default=0, blank=True, verbose_name="壓高下限"
    )  # 上下限

    Bup = models.FloatField(
        max_length=100, default=0, blank=True, verbose_name="重量上限"
    )  # 上下限
    Bdown = models.FloatField(
        max_length=100, default=0, blank=True, verbose_name="重量下限")

    A = models.CharField(
        max_length=200, verbose_name="壓高", default=None, blank=True)
    B = models.CharField(
        max_length=200, verbose_name="重量", default=None, blank=True)
    
    position = models.CharField(max_length=100,verbose_name="工位",default="")
    add_time = models.DateTimeField(verbose_name='建立時間',default=timezone.now)
    
    boolean_label=['NG_A','NG_B','brokenhat','mucosa','unfillcorner','collapse','Rscar','AtificalNg']
    verbose_label=['NG_壓高','NG_重量','脫帽','黏模','缺角','崩模','R裂','員工判斷NG']

    for blabel,vlabel in  zip(boolean_label,verbose_label):
        locals()[blabel]=models.BooleanField(default=False ,verbose_name=vlabel)
    del locals()['blabel']


    class Meta:
        verbose_name_plural = "  壓形資料"
        verbose_name = "壓形資料"
    
    def __str__(self):
        return str(self.form_process)

class FormProcess(models.Model):

    form_process = models.CharField(max_length=100, verbose_name="壓型總表", default=None,null=True)
    transfer_doc_child = models.ForeignKey(
        'information.TransferDocChild', verbose_name="子轉移表單", on_delete=models.CASCADE,null=True)
    note = models.CharField(max_length=200, verbose_name="壓型備註", default=None, null=True)
    employee_mf = models.ForeignKey(EmployeeAccount, verbose_name='製造員工', on_delete=models.CASCADE,null=True,related_name='employee_mf')
    employee_qc = models.ForeignKey(EmployeeAccount, verbose_name='測量員工', on_delete=models.CASCADE,null=True,related_name='employee_qc')
    check_time = models.DateTimeField(
        verbose_name='建立時間', default=timezone.now)
    cut_num=models.CharField(max_length=200, verbose_name="刀數", default=None, null=True)
    barrel_num = models.CharField(max_length=200, verbose_name="粉料桶號", default=None, null=True,blank=True)

    class Meta:
        verbose_name_plural = "壓形總表"
        verbose_name = "壓形總表"

    def __str__(self):
        return self.form_process
