from form_process.models import FormProcess, FormProcessData
from information.models import TransferDocChild, Product
from employees.models import EmployeeAccount
from rest_framework import status
from rest_framework.decorators import permission_classes
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
import json
import datetime



@api_view(["POST"])
@permission_classes([IsAuthenticated])
def measure_data_form(request):

    if request.method == "POST":

        input_json = json.loads(request.body)
        # get user who make request.
        username = request.user.username
        user_qc = EmployeeAccount.objects.get(username=username)
        user_mf = EmployeeAccount.objects.get(
            name=input_json["transfer_data"]["mf_employee"]
        )
        try:
            f = create_form_process(input_json, user_qc, user_mf)
            process = FormProcess.objects.filter(id=f.id)
        except Exception as e:
            print(e)
            content = {
                'message': 'Function : create_form_data', 'Error': str(e)}
            return Response(content, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            process.update(form_process="壓型總表" + str(f.id))
            create_form_data(input_json, f.id)
        except Exception as e:
            print(e)
            FormProcess.objects.get(id=f.id).delete()
            content = {
                'message': 'Function : create_form_data', 'Error': str(e)}
            return Response(content, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    print(datetime.date.today())
    content = {'message': '成功寫入'}
    return Response(content, status=status.HTTP_200_OK)


def create_form_data(input_json, form_process_id):
    # input for models => form_process.form_process
    data_num = 1
    data_num_str = str(data_num)
    while(input_json['data']['A'+data_num_str] != ""):
        form_data = {}
        form_data["form_process_id"] = form_process_id
        form_data["product_id"] = Product.objects.get(
            product=input_json["transfer_data"]["product_name"]
        ).id

        db_labels = ['A', 'B', 'position', 'Aup', 'Bup', 'Adown', 'Bdown', 'NG_A', 'NG_B', 'brokenhat', 'mucosa',
                     'unfillcorner', 'collapse', 'Rscar', 'AtificalNg']
        front_mesure_labels = ['A', 'B', 'position',
                               'A_ceiling', 'B_ceiling', 'A_floor', 'B_floor']
        front_ng_labels = ['NG_A', 'NG_B', 'brokenhat_', 'mucosa_',
                           'unfillcorner_', 'collapse_', 'Rscar_', 'AtificalNg_']
        front_labels = front_mesure_labels + front_ng_labels

        for front_label, db_label in zip(front_labels, db_labels):
            print(front_label)

            if front_label in front_mesure_labels:

                form_data[db_label] = input_json['data'].get(
                    front_label + data_num_str, None)

            if front_label in front_ng_labels:
                if front_label == 'NG_A':
                    front_label = 'A'
                if front_label == 'NG_B':
                    front_label = 'B'                
                form_data[db_label] = input_json["data"]["NG" +
                                                         data_num_str].get(front_label + data_num_str, False)

        form_data["Aup"] = input_json["data"]["A_ceiling"]
        form_data["Bup"] = input_json["data"]["B_ceiling"]
        form_data["Adown"] = input_json["data"]["A_floor"]
        form_data["Bdown"] = input_json["data"]["B_floor"]
        # form_data["note"] = input_json["data"]["note"]
        FormProcessData.objects.create(**form_data)
        data_num += 1
        data_num_str = str(data_num)


def create_form_process(input_json, user_qc, user_mf):

    form_process = {}
    form_process["transfer_doc_child_id"] = TransferDocChild.objects.get(
        PROC_ID=input_json["transfer_data"]["forming_transfer_num"]
    ).id
    form_process["employee_mf_id"] = user_mf.id
    form_process["employee_qc_id"] = user_qc.id
    form_process["form_process"] = ""
    form_process["cut_num"] = input_json["transfer_data"]["cut_num"]
    #form_process["barrel_num"] = input_json["transfer_data"]["barrel_num"]
    return FormProcess.objects.create(**form_process)

