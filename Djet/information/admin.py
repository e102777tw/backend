from django.contrib import admin
from .models import   Customers,Product,TransferDocMaster,TransferDocChild,WorkOrder
from django.apps import apps
from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html



@admin.register(Customers)
class CustomersProfile(admin.ModelAdmin):
    list_display = [field.name for field in Customers._meta.fields if field.name != "id"]

    def customer(self, obj):
        return obj.customer

@admin.register(Product)
class ProductProfile(admin.ModelAdmin):
    search_fields = ['product']
    list_display = [field.name for field in Product._meta.fields if field.name != "id" ]

  

    def product(self, obj):
        return obj.product


@admin.register(TransferDocMaster)
class TransferDocProfile(admin.ModelAdmin):
    list_display = [field.name for field in TransferDocMaster._meta.fields if field.name != "id"]

    def transfer_doc(self, obj):
        return obj.TransferDocMaster

@admin.register(TransferDocChild)
class TransferDocFormingProfile(admin.ModelAdmin):
    list_display = [field.name for field in TransferDocChild._meta.fields if field.name!="id"]

    def transfer_doc_forming(self,obj):
        return obj.TransferDocChild

@admin.register(WorkOrder)
class WorkOrdertProfile(admin.ModelAdmin):
    list_display = [field.name for field in WorkOrder._meta.fields if field.name != "id" ]
    def WorkOrder(self, obj):
        return obj.WorkOrder


