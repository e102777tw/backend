from django.apps import AppConfig


class InformationConfig(AppConfig):
    name = 'information'
    verbose_name='  基本資料'
    main_menu_index = 1


