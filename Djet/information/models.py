from django.utils import timezone
from django.db import models
from django.contrib import admin
from employees.models import EmployeeAccount
import datetime


class Customers(models.Model):

    customer = models.CharField(
        max_length=100, verbose_name="顧客", default=None
    )

    class Meta:
        verbose_name_plural = "       顧客"
        verbose_name = "顧客"

    def __str__(self):
        return self.customer

class Product(models.Model):

    group = models.CharField(
        max_length=100, verbose_name="群組", default=None, null=True)
    product = models.CharField(
        max_length=100, verbose_name="規格", default=None, unique=True, null=True)
    product_material = models.CharField(
        max_length=100, verbose_name="材質", default=None, null=True)
    customer = models.ForeignKey(
        Customers, on_delete=models.CASCADE, verbose_name="顧客", null=True)
    stars = models.CharField(
        max_length=100, verbose_name="星數", default=None, null=True)
    wire_gauge = models.CharField(
        max_length=100, verbose_name="線規格", default=None, null=True)
    coil_n_wire_length = models.CharField(
        max_length=100, verbose_name="線圈/線長", default=None, null=True)
    MHZ1 = models.CharField(
        max_length=100, verbose_name="1MHZ", default="", null=True)
    MHZ10 = models.CharField(
        max_length=100, verbose_name="10MHZ", default="", null=True)
    MHZ25 = models.CharField(
        max_length=100, verbose_name="25MHZ", default="", null=True)
    MHZ30 = models.CharField(
        max_length=100, verbose_name="30MHZ", default="", null=True)
    MHZ50 = models.CharField(
        max_length=100, verbose_name="50MHZ", default="", null=True)
    MHZ70 = models.CharField(
        max_length=100, verbose_name="70MHZ", default="", null=True)
    MHZ100 = models.CharField(
        max_length=100, verbose_name="100MHZ", default="", null=True)
    MHZ200 = models.CharField(
        max_length=100, verbose_name="200MHZ", default="", null=True)
    MHZ300 = models.CharField(
        max_length=100, verbose_name="300MHZ", default="", null=True)
    # for R type
    N = models.CharField(max_length=100, verbose_name="N",
                         default=None, null=True)
    Nmm = models.CharField(
        max_length=100, verbose_name="N(mm)", default=None, null=True)
    Amp = models.CharField(
        max_length=100, verbose_name="Amp(1)", default=None, null=True)
    Amp2 = models.CharField(
        max_length=100, verbose_name="Amp(2)", default=None, null=True)
    Q = models.CharField(max_length=100, verbose_name="Q",
                         default=None, null=True)
    L1 = models.CharField(
        max_length=100, verbose_name="L1", default=None, null=True)
    L2 = models.CharField(
        max_length=100, verbose_name="L2", default=None, null=True)
    L3 = models.CharField(
        max_length=100, verbose_name="L3", default=None, null=True)
    Thread1 = models.CharField(
        max_length=100, verbose_name="Thread 1 ", default=None, null=True)
    Thread2 = models.CharField(
        max_length=100, verbose_name="Thread 2 ", default=None, null=True)

    def __str__(self):
        return self.product

    class Meta:
        verbose_name_plural = "      規格"
        verbose_name = "規格"

class TransferDocChild(models.Model):

    PROC_ID = models.CharField(
        max_length=100, verbose_name="子移轉單號", default=None, null=True)
    TRANS_ID = models.CharField(
        max_length=100, verbose_name="移轉單號", default=None, null=True)
    SEQ_NO = models.CharField(max_length=100, default=None, null=True)
    EQP_TYPE = models.CharField(max_length=100, default=None, null=True)
    TRANS_UNIT = models.CharField(max_length=100, default=None, null=True)
    CREATE_DT = models.DateField(default=timezone.now, null=True)
    CREATE_USER = models.ForeignKey(
        EmployeeAccount, on_delete=models.CASCADE, verbose_name="創建員工", related_name='user_tdc', null=True)
    UPDATE_DT = models.CharField(max_length=100, default=None, null=True)
    UPDATE_USER = models.ForeignKey(
        EmployeeAccount, on_delete=models.CASCADE, verbose_name="創建員工", related_name='user_tdu', null=True)
    QC_RESULT = models.CharField(max_length=100, default=None, null=True)
    QC_COMMENT = models.CharField(max_length=100, default=None, null=True)
    QC_DATE = models.CharField(max_length=100, default=None, null=True)
    QC_USER = models.ForeignKey(EmployeeAccount, on_delete=models.CASCADE,
                                verbose_name="QC員工", related_name='user_tdqc', null=True)

    class Meta:
        verbose_name_plural = "child移轉單"
        verbose_name = "child移轉單"

    def __str__(self):
        return self.PROC_ID


class TransferDocMaster(models.Model):

    TRANS_ID = models.CharField(
        max_length=100, verbose_name="移轉單號", default=None, null=True)
    ORDER_ID = models.CharField(
        max_length=100, verbose_name="工令", default=None, null=True)
    PACK_ID = models.CharField(max_length=100, default=None, null=True)
    MF_AMOUNT = models.CharField(max_length=100, default=None, null=True)
    MF_MATERIAL = models.CharField(max_length=100, default=None, null=True)
    STATUS = models.CharField(
        max_length=100, verbose_name="狀態", default=None, blank=True)
    CREATE_DT = models.DateField(default=timezone.now, null=True)
    CREATE_USER = models.ForeignKey(
        EmployeeAccount, on_delete=models.CASCADE, verbose_name="創建員工", related_name='user_td', null=True)
    QC_STATUS = models.CharField(max_length=100,verbose_name="QC_STATUS",default="0000000")
    # F S Q QL N DB/MHZ Q
    product = models.ForeignKey("information.Product", on_delete=models.CASCADE, verbose_name="產品", null=True)

    class Meta:
        verbose_name_plural = "master移轉單"
        verbose_name = "master移轉單"

    def __str__(self):
        return self.TRANS_ID

class WorkOrder(models.Model):
    
    ORDER_ID = models.CharField(
        max_length=100, verbose_name="ORDER_ID", default=None)
    
    PROD_ID = models.CharField(
        max_length=100, verbose_name="PROD_ID", default=None)

    PROD_SPEC = models.CharField(
        max_length=100, verbose_name="PROD_SPEC", default=None)
    CUST_CODE = models.ForeignKey(Customers,on_delete=models.CASCADE,verbose_name="顧客",null=True)
    AMOUNT = models.CharField(
        max_length=100, verbose_name="AMOUNT", default=None)

    SHIP_DATE = models.DateField(default=timezone.now)
    MF_DATE = models.DateField(default=timezone.now)
    CREATE_DT = models.DateField(default=timezone.now)
    CREATE_USER = models.CharField(
        max_length=100, verbose_name="CREATE_USER", default=None)
    UPDATE_DT = models.CharField(
        max_length=100, verbose_name="UPDATE_DT", default=None,blank=True)
    UPDATE_USER = models.CharField(
        max_length=100, verbose_name="UPDATE_USER", default=None,blank=True)
    

    class Meta:
        verbose_name_plural = "工令"
        verbose_name = "工令"

    def __str__(self):
        return self.ORDER_ID