from information.models import Product 
from sinter_process.models import ProductSize
import json
import requests
from information.serializers import ProductSizeserializers,ProductEnserializers

def product_data(request):
    
    product_name_input = json.loads(request.body)

    product_name = product_name_input["product_data"]

    product_id= Product.objects.get(product=product_name).id

    product_size=ProductSize.objects.filter(product_id=product_id)

    product_serializer = ProductSizeserializers(
        product_size,  many=True, context={"request": request}
    )

    product_data_str = json.dumps(product_serializer.data)      #資料轉Json檔
    product_data_json = json.loads(product_data_str)            #存Json檔

    product_value = []                                          #建list來放Json的內容資料
    for key in product_data_json[0].keys():                     #只存要的資料
        product_value.append(product_data_json[0][key])
    output = []
    index = 0
    for i in range(0, len(product_value), 3):
        output.append(                                
            str(round(float(product_value[i]) + float(product_value[i + 1]),2))
            + "_"
            + str(round(float(product_value[i]) - float(product_value[i + 2]),2))
        )
        if (product_value[i]) != 0:
            index += 1

    output_json = {
        "A": output[0],
        "B": output[1],
        "C": output[2],
        "D": output[3],
        "E": output[4],
        "index": index,
        "product_en":product_en_data(product_name)
    }
    
    return output_json 

def product_en_data (product_name):

    product_en=Product.objects.get(product=product_name)

    product_en = ProductEnserializers(product_en).data
    
    if product_en["L2"] != None :
        product_en["L2"] =product_en["L2"].replace('(','')
        product_en["L2"] =product_en["L2"].replace(')','')
    
    return product_en
