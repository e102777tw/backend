# 讀取測資用 未來將沒用途

##################################################
def input_data_form(request):
    with open('data.json', 'r') as inputf:
        j = json.load(inputf)
    ans = j[1]

    for i in range(49):
        try:
            rjson = create_R_EB_product_info(ans, i)
            #pjson = create_product_info(ans, i)
            p=Product.objects.create(**rjson)
        except:
           print("重複輸入product")
        try:
            rsjson = create_R_sinter_size(ans, i)
            # sjson = create_sinter_size(ans, i)
            ProductSize.objects.create(**rsjson)
        except:
            print(i)
            #Product.objects.get(id = p.id).delete()
            print("Unexpected error:", sys.exc_info()[0])
    return JsonResponse("OK", safe=False)

def create_product_info(dic, i):
    p = {}
    num = '_'+str(i)
    db_label = ['group', 'product', 'product_material', 'stars', 'wire_gauge', 'coil_n_wire_length',
                'MHZ1', 'MHZ10', 'MHZ25', 'MHZ30', 'MHZ50', 'MHZ70', 'MHZ100', 'MHZ200', 'MHZ300']
    json_label = ['group', '規格'+num, '材質'+num, '星數'+num, '線規格'+num, '線圈/線長'+num, '1MHZ'+num, '10MHZ' +
                  num, '25MHZ'+num, '30MHZ'+num, '50MHZ'+num, '70MHZ'+num, '100MHZ'+num, '200MHZ'+num, '300MHZ'+num]
    group = dic['規格'+num].split("-")
    dic['group'] = group[0]
    c = dic['customer']
    cid = Customers.objects.get(customer=c).id
    p['customer_id'] = cid
    for index in range(len(db_label)):
        p[db_label[index]] = str(dic[json_label[index]])
    return(p)

def create_sinter_size(dic, i):
    s = {}
    num = '_'+str(i)
    db_label = [field.name for field in ProductSize._meta.fields if field.name !=
                "id" and field.name != "product"]

    for i in range(5):
        globals()[chr(65+i)] = re.split("[±,+,-]", dic[str(chr(65+i)+num)])
        if len(eval(chr(65+i))) == 2:
            eval(chr(65+i)).append(eval(chr(65+i))[1])
        if len(eval(chr(65+i))) == 1:
            eval(chr(65+i))[0]='0'
            eval(chr(65+i)).append('0')
            eval(chr(65+i)).append('0')
    l = A+B+C+D+E
    for index, i in zip(db_label, range(len(db_label))):
        s[index] = l[i]
    pid = Product.objects.get(product=dic['規格'+num]).id
    s['product_id']=pid
    return(s)

def create_R_product_info(dic, i):
    r = {}
    num = '_'+str(i)
    db_label = ['group', 'product',
                'N', 'L1', 'Thread1', 'Thread2', 'Amp', 'Nmm','L2', 'L3']
    json_label = ['group', 'Dimensions'+num,'N'+num, 'L'+num, 'Thread'+num, 'Thread 2' +
                  num, 'Amp'+num, 'N(mm)'+num,'L2'+num, 'L3'+num]
    group = dic['Dimensions'+num].split("-")
    dic['group'] = group[0]
    c = dic['customer']
    cid = Customers.objects.get(customer=c).id
    r['customer_id'] = cid
    for index in range(len(db_label)):
        r[db_label[index]] = str(dic[json_label[index]])

    return(r)
def create_R_EB_product_info(dic, i):
    r = {}
    num = '_'+str(i)
    db_label = ['group', 'product',
                'N', 'L1', 'Thread1', 'Amp', 'Amp2', 'Nmm','L2', 'L3','Q']
    json_label = ['group', 'Dimensions'+num,'N'+num, 'L'+num, 'Thread'+num,'Amp(1)'+num,'Amp(2)'+num, 'N(mm)'+num,'L2'+num, 'L3'+num,"Q"+num]
    group = dic['Dimensions'+num].split("-")
    dic['group'] = group[0]
    c = dic['customer']
    cid = Customers.objects.get(customer=c).id
    r['customer_id'] = cid
    for index in range(len(db_label)):
        r[db_label[index]] = str(dic[json_label[index]])

    return(r)


def create_R_sinter_size(dic,i):
    rs = {}
    num = '_'+str(i)
    db_label = [field.name for field in ProductSize._meta.fields if field.name !=
                "id" and field.name != "product"]

    for i in range(5):
        try:
            globals()[chr(65+i)] = re.split("[±,+,-]", dic[str(chr(65+i)+num)])
            print(A)
        except:
            globals()[chr(65+i)] =[]
            eval(chr(65+i)).append('0')
        if len(eval(chr(65+i))) == 2:
            eval(chr(65+i)).append(eval(chr(65+i))[1])
        if len(eval(chr(65+i))) == 1:
            eval(chr(65+i))[0]='0'
            eval(chr(65+i)).append('0')
            eval(chr(65+i)).append('0')
    l = A+B+C+D+E
    for index, i in zip(db_label, range(len(db_label))):
        rs[index] = l[i]
    pid = Product.objects.get(product=dic['Dimensions'+num]).id
    rs['product_id']=pid
    return(rs)
##################################################