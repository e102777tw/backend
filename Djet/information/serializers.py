from rest_framework import serializers
from sinter_process.models import ProductSize
from information.models import Product

class ProductSizeserializers(serializers.ModelSerializer):
    class Meta:
        model=ProductSize
        fields= ["A","Aup","Adown","B","Bup","Bdown","C","Cup","Cdown","D","Dup","Ddown","E","Eup","Edown"]


class ProductEnserializers(serializers.ModelSerializer):
    class Meta:
        model=Product
        fields= ["wire_gauge","coil_n_wire_length","MHZ1","MHZ10","MHZ25","MHZ30","MHZ50","MHZ70","MHZ100","MHZ200","MHZ300","N","Nmm","Amp","Amp2","Q","L2","L3","Thread1","Thread2"]
