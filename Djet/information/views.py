from .models import Product, Customers, TransferDocMaster,TransferDocChild,WorkOrder
from sinter_process.models import ProductSize
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import permission_classes
from rest_framework.decorators import api_view
# function import  /get_object_id.py /create_object.py
from .myFunction import product_data
import json
import datetime
import re
import sys
# 產品資料

import logging

# Get an instance of a logger
logger = logging.getLogger('standard_logger')


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def post_product_data(request):
    output_json = product_data.product_data(request)
    logger.error('Your message is here.')
    return JsonResponse(output_json, safe=False)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_product_group(request):
    product_group = {}
    group_json = Product.objects.all().values('group').distinct()  # 只取 group 然後去重複
    for key in range(len(group_json)):
        product_group[group_json[key]['group']] = list(Product.objects.filter(
            group=group_json[key]['group']).values_list('product', flat=True))
    return JsonResponse(product_group, safe=False)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_customers_name(request):
    customers_json = list(Customers.objects.all().values())
    return JsonResponse(customers_json, safe=False)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_transfer_doc_master_name(request):
    transfer_doc_json = list(TransferDocMaster.objects.all().values())
    return JsonResponse(transfer_doc_json, safe=False)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_transfer_doc_child_name(request):
    transfer_doc_json = list(TransferDocChild.objects.all().values())
    return JsonResponse(transfer_doc_json, safe=False)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_transfer_doc_TF_child_name(request):
    transfer_doc_TF_json = list(TransferDocChild.objects.filter(PROC_ID__icontains='TF').values())
    return JsonResponse(transfer_doc_TF_json, safe=False)
    
@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_transfer_doc_TS_child_name(request):
    transfer_doc_TS_json = list(TransferDocChild.objects.filter(PROC_ID__icontains='TS').values())  
    return JsonResponse(transfer_doc_TS_json, safe=False)



@api_view(['GET'])
@permission_classes([IsAuthenticated])
def  get_work_order_name(request):
    Work_order_json=list(WorkOrder.objects.all().values())
    return JsonResponse(Work_order_json, safe=False) 
