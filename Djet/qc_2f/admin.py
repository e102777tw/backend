from django.contrib import admin
from .models import QcSecondFloorSize,QcSecondFloorEn
from django.urls import reverse
from django.utils.html import format_html




@admin.register(QcSecondFloorSize)
class QcSecondFloorSizeProfile(admin.ModelAdmin):
    list_display = [field.name for field in QcSecondFloorSize._meta.fields if field.name != 'id']


@admin.register(QcSecondFloorEn)
class QcSecondFloorEnProfile(admin.ModelAdmin):
    list_display = [field.name for field in QcSecondFloorEn._meta.fields if field.name != 'id']