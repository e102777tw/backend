from django.apps import AppConfig


class Qc2FConfig(AppConfig):
    name = 'qc_2f'
