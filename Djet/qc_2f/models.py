from django.db import models
from django.utils import timezone
from employees.models import EmployeeAccount
# Create your models here.

class QcSecondFloorEn(models.Model):
    transfer_doc_child = models.ForeignKey(
        'information.TransferDocChild', verbose_name="子轉移表單", on_delete=models.CASCADE,null=True)
    employee=models.ForeignKey(EmployeeAccount,verbose_name='員工',on_delete=models.CASCADE,null=True)
    check_time=models.DateTimeField(verbose_name='建立時間',default=timezone.now)
    labels = ['MHZ1','MHZ10','MHZ25','MHZ30','MHZ50','MHZ70','MHZ100','MHZ200','MHZ300','N','L','Q','note']
    verbose_labels = ['1MHZ','10MHZ','25MHZ','30MHZ','50MHZ','70MHZ','100MHZ','200MHZ','300MHZ','N','L','Q','note']
    for label,verbose_label in zip(labels,verbose_labels) :
        locals()[label] = models.CharField(max_length=100, verbose_name=verbose_label, default=None,null=True)
    ng_labels = ['MHZ1_NG','MHZ10_NG','MHZ25_NG','MHZ30_NG','MHZ50_NG','MHZ70_NG','MHZ100_NG','MHZ200_NG','MHZ300_NG','N_NG','L_NG']
    for ng_label in ng_labels:
        locals()[ng_label] = models.BooleanField(max_length=255,null=True)
    class Meta:
        verbose_name_plural="qc_2f_電性"
        verbose_name="qc_2f_電性"

class QcSecondFloorSize(models.Model):
    transfer_doc_child = models.ForeignKey(
        'information.TransferDocChild', verbose_name="子轉移表單", on_delete=models.CASCADE,null=True)
    employee=models.ForeignKey(EmployeeAccount,verbose_name='員工',on_delete=models.CASCADE,null=True)
    check_time=models.DateTimeField(verbose_name='建立時間',default=timezone.now)
    labels = [chr(65 + label)for label in range(5)]
    labels.append('note')
    for label in labels:
        locals()[label] = models.CharField(max_length=255,null=True)
    boolean_label=['NG_A','NG_B','NG_C','NG_D','NG_E']
    verbose_label=['NG_A','NG_B','NG_C','NG_D','NG_E']
    for blabel,vlabel in  zip(boolean_label,verbose_label):
        locals()[blabel]=models.BooleanField(default=False ,verbose_name=vlabel)
    del locals()['blabel']

    class Meta:
        verbose_name_plural="qc_2f資料"
        verbose_name="qc_2f資料"

