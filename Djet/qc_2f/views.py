from django.shortcuts import render
from .models import QcSecondFloorSize, QcSecondFloorEn
from information.models import Product
from rest_framework.decorators import permission_classes
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from employees.models import EmployeeAccount
from rest_framework import status
from rest_framework.response import Response
import json


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def measure_data_qc_2f_size(request):

    if request.method == "POST":
        input_json = json.loads(request.body)
        print(request.body)
        # get user that make request.
        user = EmployeeAccount.objects.get(username=request.user.username)
        try:
            create_qc_2f_size(input_json, user)
        except Exception as e :
            print(e)
            content = {'message': 'Function : create_qc_2f_size','Error':str(e)}
            return Response(content, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        content = {'message': '成功寫入'}
    return Response(content, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def measure_data_qc_2f_en(request):

    if request.method == "POST":
        input_json = json.loads(request.body)
        print(request.body)
        # get user that make request.
        user = EmployeeAccount.objects.get(username=request.user.username)
        try:
            create_qc_2f_en(input_json, user)
        except Exception as e:
            print(e)
            content = {
                'message': 'Function : create_qc_2f_en', 'Error': str(e)}
            return Response(content, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        content = {'message': '成功寫入'}
    return Response(content, status=status.HTTP_200_OK)


def create_qc_2f_size(input_json, user):
    data_num = 1
    data_num_str = str(data_num)
    while(input_json['data']['A'+data_num_str] != ""):

        qc_2f_size = {}

        qc_2f_size['transfer_doc_child_id'] = TransferDocChild.objects.get(
            PROC_ID=input_json['transfer_data']['sinter_transfer_num']).id
        qc_2f_size['employee_id'] = user.id
        qc_2f_size['note'] = input_json['data'].get('note' + data_num_str, None)
        qc_2f_size['A'] = input_json['data'].get('A' + data_num_str, None)
        qc_2f_size['B'] = input_json['data'].get('B' + data_num_str, None)
        qc_2f_size['C'] = input_json['data'].get('C' + data_num_str, None)
        qc_2f_size['D'] = input_json['data'].get('D' + data_num_str, None)
        qc_2f_size['E'] = input_json['data'].get('E' + data_num_str, None)
        qc_2f_size['NG_A'] = input_json['data']['NG' +
                                                data_num_str].get('A'+data_num_str, False)
        qc_2f_size['NG_B'] = input_json['data']['NG' +
                                                data_num_str].get('B'+data_num_str, False)
        qc_2f_size['NG_C'] = input_json['data']['NG' +
                                                data_num_str].get('C'+data_num_str, False)
        qc_2f_size['NG_D'] = input_json['data']['NG' +
                                                data_num_str].get('D'+data_num_str, False)
        qc_2f_size['NG_E'] = input_json['data']['NG' +
                                                data_num_str].get('E'+data_num_str, False)

        QcSecondFloorSize.objects.create(**qc_2f_size)
        data_num += 1
        data_num_str = str(data_num)


def create_qc_2f_en(input_json, user):
    data_num = 1
    data_num_str = "_" + str(data_num)
    while(input_json['data']['MHZ1'+data_num_str] != ""):
        qc_2f_en = {}
        qc_2f_en['TransferDocChild_id'] = TransferDocChild.objects.get(
            PROC_ID=input_json['transfer_data']['sinter_transfer_num']).id
        qc_2f_en['employee_id'] = user.id
        qc_2f_en['note'] = input_json['data'].get('note' + data_num_str, None)
        qc_2f_en['Q'] = input_json['data'].get('Q' + data_num_str, None)

        front_en_labels = ['MHZ1', 'MHZ10', 'MHZ25', 'MHZ30',
                           'MHZ50', 'MHZ70', 'MHZ100', 'MHZ200', 'MHZ300', 'N', 'L']
        en_labels = ['MHZ1', 'MHZ10', 'MHZ25', 'MHZ30',
                     'MHZ50', 'MHZ70', 'MHZ100', 'MHZ200', 'MHZ300', 'N', 'L']
        ng_labels = ['MHZ1_NG', 'MHZ10_NG', 'MHZ25_NG', 'MHZ30_NG',
                     'MHZ50_NG', 'MHZ70_NG', 'MHZ100_NG', 'MHZ200_NG', 'MHZ300_NG', 'N_NG', 'L_NG']

        for en, front in zip(en_labels, front_en_labels):
            qc_2f_en[en] = input_json['data'].get(front+data_num_str, None)
        for ng, front in zip(ng_labels, front_en_labels):
            qc_2f_en[ng] = input_json['data']['NG' +
                                              str(data_num)].get(front+data_num_str, False)

        QcSecondFloorEn.objects.create(**qc_2f_en)
        data_num += 1
        data_num_str = "_" + str(data_num)
