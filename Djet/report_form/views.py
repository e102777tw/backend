from django.shortcuts import render
from form_process.models import Machine, FormProcess, FormProcessData
from information.models import Customers, TransferDocChild, Product
from sinter_process.models import Machine, ProductSize, SinterProcessData, SinterProcess
from rest_framework import status
from rest_framework.decorators import permission_classes
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from django.http import JsonResponse, HttpResponse
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
import json
import datetime
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt

# Create your views here.


@csrf_exempt
def data_form(request):
    # if request.method == "POST":
    # input_json = json.loads(request.body)

    form_data = {}
    sinter_data = {}
    select = {
        # "select": "1", "timeup": "2020-12-10", "timedown": "2020-12-11", #時間
        "select": "2", "Transfer_doc": "TS123"  # 子移轉單

    }
    if select["select"] == "1":
        form = FormProcess.objects.filter(check_time__range=[
                                           select['timeup'], select['timedown']])  # 壓型
        sinter = SinterProcess.objects.filter(check_time__range=[
            select['timeup'], select['timedown']])  # 燒製

    # form = FormProcess.objects.filter(TransferDocChild=[select['Transfer_doc']]).values_list('id', flat=True)  #壓型

    elif select["select"] == "2":
        form = FormProcess.objects.filter(
            TransferDocChild=TransferDocChild.objects.get(PROC_ID=select['Transfer_doc']).id)
        sinter = SinterProcess.objects.filter(
            TransferDocChild=TransferDocChild.objects.get(PROC_ID=select['Transfer_doc']).id)

    for i in range(len(form)):
        form_data[str(form[i])] = list(FormProcessData.objects.prefetch_related(  # 壓型
            'form_process').filter(form_process_id=form[i]).values())
    for i in range(len(sinter)):
        sinter_data[str(sinter[i])] = list(SinterProcessData.objects.prefetch_related(  # 燒製
            'sinter_process').filter(sinter_process_id=sinter[i]).values())

    return JsonResponse((form_data, sinter_data), safe=False)
