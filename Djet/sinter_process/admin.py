from django.contrib import admin
from .models import Machine, SinterProcess,SinterProcessData,ProductSize
from django.apps import apps
from django.contrib import admin
from employees.models import EmployeeAccount
from django.urls import reverse
from django.utils.html import format_html



@admin.register(Machine)
class MachineProfile(admin.ModelAdmin):
    list_display = [field.name for field in Machine._meta.fields if field.name != 'id' if field.name != 'id']

    def machine(self, obj):
        return obj.machine


@admin.register(ProductSize)
class ProductProfile(admin.ModelAdmin):
    list_display = [field.name for field in ProductSize._meta.fields if field.name != "id" ]

    fieldsets = (  
        (
            "General Information",
            {
                "fields": (
                    ("product",),
                    ("A", "Aup", "Adown",),
                    ("B", "Bup", "Bdown",),
                    ("C", "Cup", "Cdown",),
                    ("D", "Dup", "Ddown",),
                    ("E", "Eup", "Edown",),
                ),
            },
        ),
    )
    def product(self, obj):
        return obj.ProductSize

@admin.register(SinterProcess)
class Sinter_Process_Profile(admin.ModelAdmin):
    
    date_hierarchy = 'check_time'
    # list_display = [field.name for field in SinterProcess._meta.fields if field.name != 'id' and field.name !=
    #                 'employee'and field.name!='product_size']
    list_display = [field.name for field in SinterProcess._meta.fields if  field.name !=
                    'employee'and field.name!='product_size']
    list_display.append('link_to_employee')
    list_display.append('link_to_ps')
    #if u need search for foreignkey use __
    search_fields=['sinter_process']
    
    readonly_fields=[field.name for field in SinterProcess._meta.fields if field.name != 'id']

    def sinter_process(self, obj):
        return obj.sinter_process

    def link_to_employee(self, obj):
        link=reverse("admin:employees_employeeaccount_change", args=[obj.employee.id]) 
        return format_html('<a href="{}">{}</a>', link,obj.employee.name)
    def link_to_ps(self, obj):
        link=reverse("admin:sinter_process_product_size_change", args=[obj.product_size.id]) 
        return format_html('<a href="{}">{}</a>', link,obj.product_size.product)
    link_to_employee.short_description='員工'
    link_to_employee.allow_tags=True
    link_to_ps.short_description='燒製上下限'
    link_to_ps.allow_tags=True

    def has_add_permission(self, requsest ,obj=None):
        return False


@admin.register(SinterProcessData)
class Sinter_Process_Data_Profile(admin.ModelAdmin):
        list_display = [field.name for field in SinterProcessData._meta.fields if field.name != 'id']


