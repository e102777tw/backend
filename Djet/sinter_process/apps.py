from django.apps import AppConfig


class SinterProcessConfig(AppConfig):
    name = 'sinter_process'
    verbose_name=' 燒製階段'
    main_menu_index = 0


