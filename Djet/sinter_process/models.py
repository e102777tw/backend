from django.db import models
from django.contrib import admin
from django.core.validators import (
    MaxValueValidator,
    MinValueValidator,
    RegexValidator,
    ValidationError,
)
import django.utils.timezone as timezone
from employees.models import EmployeeAccount




class Machine(models.Model):

    machine = models.CharField(
        max_length=100, verbose_name="機器", default=None)
    machine_serial = models.CharField(max_length=50, verbose_name="機器編號", default=None)
    COTTOR_CHOICES = zip(range(1, 4), range(1, 4))
    cottor_number = models.PositiveIntegerField(
        default=1,
        validators=[MaxValueValidator(3), MinValueValidator(1)],
        verbose_name="模具刀數",
        choices=COTTOR_CHOICES,
    )

    class Meta:
        verbose_name_plural = "    燒製機器"
        verbose_name = "燒製機器"

    def __str__(self):
        return self.machine



class ProductSize(models.Model):

    product = models.OneToOneField("information.Product",verbose_name="產品",on_delete=models.CASCADE,related_name="sinter_product")

    A = models.FloatField(max_length=100, verbose_name="A", default=0)
    Aup = models.FloatField(
        max_length=100, default=0, blank=True, verbose_name="A上限"
    )  # 上下限
    Adown = models.FloatField(
        max_length=100, default=0, blank=True, verbose_name="A下限"
    )  # 上下限

    B = models.FloatField(max_length=100, verbose_name="B", default=0)
    Bup = models.FloatField(
        max_length=100, default=0, blank=True, verbose_name="B上限"
    )  # 上下限
    Bdown = models.FloatField(
        max_length=100, default=0, blank=True, verbose_name="B下限"
    )  # 上下限

    C = models.FloatField(max_length=100, verbose_name="C", default=0)
    Cup = models.FloatField(
        max_length=100, default=0, blank=True, verbose_name="C上限"
    )  # 上下限
    Cdown = models.FloatField(
        max_length=100, default=0, blank=True, verbose_name="C下限"
    )  # 上下限

    D = models.FloatField(max_length=100, verbose_name="D", default=0)
    Dup = models.FloatField(
        max_length=100, default=0, blank=True, verbose_name="D上限"
    )  # 上下限
    Ddown = models.FloatField(
        max_length=100, default=0, blank=True, verbose_name="D下限"
    )  # 上下限

    E = models.FloatField(max_length=100, verbose_name="E", default=0)
    Eup = models.FloatField(
        max_length=100, default=0, blank=True, verbose_name="E上限"
    )  # 上下限
    Edown = models.FloatField(
        max_length=100, default=0, blank=True, verbose_name="E下限"
    )  # 上下限

    class Meta:
        verbose_name_plural = "燒製規格上下限"
        verbose_name = "燒製規格上下限"
    def __str__(self):
        return "燒製規格上下限"

class SinterProcessData(models.Model):


    sinter_process=models.ForeignKey('sinter_process.SinterProcess',on_delete=models.CASCADE,null=True)
    labels = [chr(65 + label)for label in range(5)]
    for label in labels:
        locals()[label] = models.CharField(max_length=255,null=True)
    boolean_label=['NG_A','NG_B','NG_C','NG_D','NG_E']
    verbose_label=['NG_A','NG_B','NG_C','NG_D','NG_E']
    for blabel,vlabel in  zip(boolean_label,verbose_label):
        locals()[blabel]=models.BooleanField(default=None ,verbose_name=vlabel,null=True)
    del locals()['blabel']

    class Meta:
        verbose_name_plural=" 燒製資料"
        verbose_name=" 燒製資料"
class SinterProcess(models.Model):
    LAYER_CHOICES = zip( range(1,5), range(1,5) )
    STAGE_CHOICES ={('1', 'Early'),('2', 'Middle'),('3', 'Last')}
    
    transfer_doc_child = models.ForeignKey('information.TransferDocChild',verbose_name="子轉移表單",on_delete=models.CASCADE,null=True)
    sinter_process = models.CharField(max_length=100,verbose_name="燒製總表",default=None,null=True)
    stage_id = models.CharField(max_length=100,verbose_name="燒製階段",default=None,choices=STAGE_CHOICES,null=True)
    layer = models.PositiveIntegerField(verbose_name="燒製階層",default=None,choices=LAYER_CHOICES)
    check_time = models.DateTimeField(verbose_name='建立時間',default=timezone.now)
    employee = models.ForeignKey(EmployeeAccount,verbose_name='員工',on_delete=models.CASCADE,null=True)       
    product_size = models.ForeignKey('sinter_process.ProductSize',verbose_name="上下限",on_delete=models.CASCADE)       
    bend = models.CharField(max_length=200, verbose_name="彎曲", default=None, null=True)
    note = models.CharField(max_length=200, verbose_name="燒製備註", default=None, null=True)

    class Meta:
        verbose_name_plural=" 燒製總表"
        verbose_name=" 燒製總表"
    
    def __str__(self):
        return self.sinter_process













    










    
  


