from rest_framework import serializers
from .models import Product

class Productserializers(serializers.ModelSerializer):
    class Meta:
        model=Product
        fields= ["A","Aup","Adown","B","Bup","Bdown","C","Cup","Cdown","D","Dup","Ddown","E","Eup","Edown"]

class ProductGserializers(serializers.ModelSerializer):
    class Meta:
        model=Product
        fields= ["group","product"]
