from .models import Machine, SinterProcess, SinterProcessData, ProductSize
from employees.models import EmployeeAccount
from information.models import Product, TransferDocChild
from django.http import JsonResponse, HttpResponse
from rest_framework import status
from rest_framework.response import Response
from django.utils import timezone
from rest_framework.decorators import permission_classes
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
import json


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def measure_data_sinter(request):

    if request.method == "POST":
        input_json = json.loads(request.body)
        # get user that make request.
        user = EmployeeAccount.objects.get(username=request.user.username)
        try:
            s = create_sinter_process(input_json, user)
            process = SinterProcess.objects.filter(id=s.id)
            process.update(sinter_process='燒製總表'+str(s.id))
        except Exception as e :
            print(e)
            content = {'message': 'Function : create_sinter_process','Error':str(e)}
            return Response(content, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        try:
            create_sinter_data(input_json, s.id)
        except Exception as e :
            print(e)
            SinterProcess.objects.get(id = s.id).delete()
            content = {'message': 'Function : create_sinter_process_data','Error':str(e)}
            return Response(content, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        content = {'message': '成功寫入'}
    return Response(content, status=status.HTTP_200_OK)

def create_sinter_process(input_json, user_qc):
    sinter_process = {}
    # get product id because we need to get product_size via later.
    product = Product.objects.get(
        product=input_json["transfer_data"]["product_name"]).id
    sinter_process['stage_id'] = input_json['transfer_data']['stage_choice']
    # sinter_process['bend'] = input_json['data']['bend']
    sinter_process['layer'] = input_json['data']['layer']
    sinter_process['employee_id'] = user_qc.id
    sinter_process['product_size_id'] = ProductSize.objects.get(
        product_id=product).id
    sinter_process['transfer_doc_child_id'] = TransferDocChild.objects.get(
        PROC_ID=input_json['transfer_data']['sinter_transfer_num']).id
    sinter_process['sinter_process'] = ''
    return SinterProcess.objects.create(**sinter_process)


def create_sinter_data(input_json, sinter_process_id):
    data_num = 0
    while(True):
        data_num_str = str(data_num+1)
        sinter_data = {}
        if(input_json['data']['A'+str(data_num+1)] == ""):
            break
        sinter_data['sinter_process_id'] = sinter_process_id
        # sinter_data['note'] = input_json['data']['note']
        sinter_data['A'] = input_json['data'].get('A'+data_num_str, None)
        sinter_data['B'] = input_json['data'].get('B'+data_num_str, None)
        sinter_data['C'] = input_json['data'].get('C'+data_num_str, None)
        sinter_data['D'] = input_json['data'].get('D'+data_num_str, None)
        sinter_data['E'] = input_json['data'].get('E'+data_num_str, None)
        sinter_data['NG_A'] = input_json['data']['NG' +
                                                 data_num_str].get('A'+data_num_str, False)
        sinter_data['NG_B'] = input_json['data']['NG' +
                                                 data_num_str].get('B'+data_num_str, False)
        sinter_data['NG_C'] = input_json['data']['NG' +
                                                 data_num_str].get('C'+data_num_str, False)
        sinter_data['NG_D'] = input_json['data']['NG' +
                                                 data_num_str].get('D'+data_num_str, False)
        sinter_data['NG_E'] = input_json['data']['NG' +
                                                 data_num_str].get('E'+data_num_str, False)
        SinterProcessData.objects.create(**sinter_data)
        data_num += 1


